const Menu = require('../models/menuModel');
const catchAsync = require('../utils/catchAsync');

module.exports.getAllItems = catchAsync(async (req, res, next) => {
  const menu = await Menu.find();

  res.status(200).json({
    status: 'success',
    data: {
      menu,
    },
  });
});

module.exports.addItem = catchAsync(async (req, res, next) => {
  const item = await Menu.create(req.body);
  res.status(201).json({
    status: 'success',
    data: {
      item,
    },
  });
});

module.exports.updateItem = catchAsync(async (req, res, next) => {
  const newItem = await Menu.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });
  res.status(201).json({
    status: 'success',
    data: {
      newItem,
    },
  });
});

module.exports.getItem = catchAsync(async (req, res, next) => {
  const item = await Menu.findById(req.params.id);
  res.status(200).json({
    status: 'success',
    data: {
      item,
    },
  });
});

module.exports.deleteItem = catchAsync(async (req, res, next) => {
  const item = await Menu.findByIdAndDelete(req.params.id);

  res.status(204).json({
    status: 'success',
    data: {
      item,
    },
  });
});
