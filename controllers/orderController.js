const Order = require('../models/orderModel');
const catchAsync = require('../utils/catchAsync');

module.exports.createOrder = catchAsync(async (req, res, next) => {
  const order = await Order.create(req.body);

  res.status(201).json({
    status: 'success',
    data: {
      order,
    },
  });
});

module.exports.getAllOrders = catchAsync(async (req, res, next) => {
  const orders = await Order.find();
  res.status(200).json({
    status: 'success',
    data: {
      orders,
    },
  });
});

module.exports.getOrderById = catchAsync(async (req, res, next) => {
  const order = await Order.findById(req.params.id);
  res.status(200).json({
    status: 'success',
    data: {
      order,
    },
  });
});

module.exports.getOrdersByDate = catchAsync(async (req, res, next) => {
  const orders = await Order.find({
    timed: { $gte: new Date(req.params.date) },
  });
  res.status(200).json({
    status: 'success',
    results: orders.length,
    data: {
      orders,
    },
  });
});
